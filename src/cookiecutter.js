import prompt from 'prompt'
import process from 'process'
import os from 'os'
import git from 'simple-git'
import randomstring from 'randomstring'
import fs from 'fs'
import glob from 'glob'
import path from 'path'
import mkdirp from 'mkdirp'

let repo = process.argv[2]

if (! repo) {
  console.error(`
  Usage:
      cookiecutter [https://gitlab.com/stenote/cookiecutter-es6-repo]
  `)
  process.exit()
}

// 1. git clone https://gitlab.com/stenote/cookiecutter-es6-repo
// 2. 加载 requie('cookiecutter.json')
// 3. 字符串/目录替换
// 4. 项目 copy

let tmpDir = os.tmpdir()

let G = new git(tmpDir)
let repoDir = randomstring.generate(8)

G.clone(repo, repoDir, () => {
  fs.readFile(`${tmpDir}/${repoDir}/cookiecutter.json`, 'utf8', (err, data) => {

    let config = JSON.parse(data);
    let prompts = [];

    Object.entries(config).forEach(([key, value]) => {

      prompts.push({
        name: key,
        description: key,
        default: value ? value : ''
      });
    });

    prompt.message = 'cookiecutter';
    prompt.start();

    prompt.get(prompts, (err, result) => {

      // 构建 '{{cookiecutter.name}}' : 'hello' 结构

      let cookiecutterMap = {};
      Object.entries(result).forEach(([key, value]) => {
        cookiecutterMap[`{{cookiecutter.${key}}}`] = value
      })
      
      
      glob(`${tmpDir}/${repoDir}/**/`, (err, dirs) => {

        dirs = dirs.map((dir) => {
          return path.relative(`${tmpDir}/${repoDir}/`, dir)
        })

        dirs.forEach((dir) => {
          //  目录内容替换
          Object.entries(cookiecutterMap).forEach(([key, value]) => {
            dir = dir.replace(key, value)
          })
          
          if (dir) {
            // 创建目录
            mkdirp(dir, () => {
            })
          }
        })
      })

      glob(`${tmpDir}/${repoDir}/**`, {nodir: true},  (err, files) => {
        // 遍历文件内容
        files.forEach((file) => {
          fs.readFile(file, (err, data) => {
            // 文件内容替换
            data = data.toString()
            Object.entries(cookiecutterMap).forEach(([key, value]) => {
              data = data.replace(key, value)
            })

            // 相对路径进行文件写入
            file = path.relative(`${tmpDir}/${repoDir}`, file)

            // 对文件路径也进行 map 替换
            Object.entries(cookiecutterMap).forEach(([key, value]) => {
              file = file.replace(key, value)
            })

            fs.open(file, 'a+', (err, fd) => {
              // 写入文件
              fs.write(fd, data, () => {
                // nothing
              })
            })
          })
        })
      })

    })
  })
})
