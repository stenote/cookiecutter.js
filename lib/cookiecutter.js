'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _prompt = require('prompt');

var _prompt2 = _interopRequireDefault(_prompt);

var _process = require('process');

var _process2 = _interopRequireDefault(_process);

var _os = require('os');

var _os2 = _interopRequireDefault(_os);

var _simpleGit = require('simple-git');

var _simpleGit2 = _interopRequireDefault(_simpleGit);

var _randomstring = require('randomstring');

var _randomstring2 = _interopRequireDefault(_randomstring);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _glob = require('glob');

var _glob2 = _interopRequireDefault(_glob);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var repo = _process2.default.argv[2];

if (!repo) {
  console.error('\n  Usage:\n      cookiecutter [https://gitlab.com/stenote/cookiecutter-es6-repo]\n  ');
  _process2.default.exit();
}

// 1. git clone https://gitlab.com/stenote/cookiecutter-es6-repo
// 2. 加载 requie('cookiecutter.json')
// 3. 字符串/目录替换
// 4. 项目 copy

var tmpDir = _os2.default.tmpdir();

var G = new _simpleGit2.default(tmpDir);
var repoDir = _randomstring2.default.generate(8);

G.clone(repo, repoDir, function () {
  _fs2.default.readFile(tmpDir + '/' + repoDir + '/cookiecutter.json', 'utf8', function (err, data) {

    var config = JSON.parse(data);
    var prompts = [];

    Object.entries(config).forEach(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          key = _ref2[0],
          value = _ref2[1];

      prompts.push({
        name: key,
        description: key,
        default: value ? value : ''
      });
    });

    _prompt2.default.message = 'cookiecutter';
    _prompt2.default.start();

    _prompt2.default.get(prompts, function (err, result) {

      // 构建 '{{cookiecutter.name}}' : 'hello' 结构

      var cookiecutterMap = {};
      Object.entries(result).forEach(function (_ref3) {
        var _ref4 = _slicedToArray(_ref3, 2),
            key = _ref4[0],
            value = _ref4[1];

        cookiecutterMap['{{cookiecutter.' + key + '}}'] = value;
      });

      (0, _glob2.default)(tmpDir + '/' + repoDir + '/**/', function (err, dirs) {

        dirs = dirs.map(function (dir) {
          return _path2.default.relative(tmpDir + '/' + repoDir + '/', dir);
        });

        dirs.forEach(function (dir) {
          //  目录内容替换
          Object.entries(cookiecutterMap).forEach(function (_ref5) {
            var _ref6 = _slicedToArray(_ref5, 2),
                key = _ref6[0],
                value = _ref6[1];

            dir = dir.replace(key, value);
          });

          if (dir) {
            // 创建目录
            (0, _mkdirp2.default)(dir, function () {});
          }
        });
      });

      (0, _glob2.default)(tmpDir + '/' + repoDir + '/**', { nodir: true }, function (err, files) {
        // 遍历文件内容
        files.forEach(function (file) {
          _fs2.default.readFile(file, function (err, data) {
            // 文件内容替换
            data = data.toString();
            Object.entries(cookiecutterMap).forEach(function (_ref7) {
              var _ref8 = _slicedToArray(_ref7, 2),
                  key = _ref8[0],
                  value = _ref8[1];

              data = data.replace(key, value);
            });

            // 相对路径进行文件写入
            file = _path2.default.relative(tmpDir + '/' + repoDir, file);

            // 对文件路径也进行 map 替换
            Object.entries(cookiecutterMap).forEach(function (_ref9) {
              var _ref10 = _slicedToArray(_ref9, 2),
                  key = _ref10[0],
                  value = _ref10[1];

              file = file.replace(key, value);
            });

            _fs2.default.open(file, 'a+', function (err, fd) {
              // 写入文件
              _fs2.default.write(fd, data, function () {
                // nothing
              });
            });
          });
        });
      });
    });
  });
});